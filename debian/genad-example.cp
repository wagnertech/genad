#!/bin/bash
set -e

mkdir -p $1/usr/share/php/genadex
cp -r example/* $1/usr/share/php/genadex/

mkdir -p $1/etc/apache2/sites-available
cp example/etc/genadex.apache2 $1/etc/apache2/sites-available/genadex.conf

