# Generic Admin


## Description
Generic Administration. A framework providing backend and frontend for maintaining business data in a MySQL data base. The database is accessed via propel-orm (see (http://propelorm.org/Propel/)).

Via GenAd-GUI data can be maintained. GUI usage is opional, GenAd can directly be integrated into your application.

## Installation
The simplest way to install Generic Administration on a debianoid system is to use the prebuild debian packages. See https://hilfe.wagnertech.de/index.php/installation-von-wagnertech-software/

If you want to integrate this software into your project use all code below the php directory.

## Authors and acknowledgment
F\. Eisenhauer, M.J.M. Wagner

## License
see LICENCE file

# Basic Features

## Backend Features

### Filtering

The method `GenericAdmin::getInstanceList()` has a parameter `req_filt`. If not `null` a XML document is expected having the following format:

```xml
<and>
  <item co="equal">
    <att>value</att>
  </item>
</and>
```

Further operators are `<or>` and `<not>`. Further comparator (`co`) attribute values are
`present`, `less` and `greater`. `att` is the name of the attribute and `value` its value.

Example:<br>
*Project*: SystemAlarming<br>
*Test*: TestBug8<br>
*Test step*: Alarmbilanz überprüfen

### Odering

The method `GenericAdmin::getInstanceList()` has a parameter `odering`. If not `null` a XML document is expected having the following format:

```xml
<oder>
  <asc>att1</asc>
  <desc>att2</desc>
</oder>
```

The result is ordered according attribute `att1` in ascending oder, then according `att2` in decending order.

Example:<br>
*Project*: mTicket<br>
*Test*: BackEnd<br>
*Test step*: Odering

## Frontend Features

# How to use Generic Admin

## Define database model
The database model is defined with the propel xml definition. See (http://propelorm.org/Propel/).

Modelling hints:
- Propel needs an auto-increment id field
- For optimistc locking a mod(ification) field should be added to entity classes.
- Enum types (e.g. for gui choices) are modelled as integer fields.
- more modelling hints see in the Attributes section

Build PHP classes with `propel-gen`

Add save methods to the built propel PHP classes:
```
public function save(PropelPDO $con = null)
{
	if ($this->isNew() || $this->mod >= 999999) {
		$this->setMod(0);
	} else {
		$this->setMod($this->mod + 1);
	}
	return parent::save($con);
}
```

## Define GenadData.php
This file defines
- how attributes behave in PHP
- how classes/attributes look like on the GUI

GenadData.php has two sections.
- First the entities are defined, that can be maintained. For details see the OC definition section.
- Second all attributes and relations between the entities are definied. For details see Attributes section.

## OC definition
In the example application there are N object classes (OC):
- MOC1, MOC2, MOC4, MOC6 managed object classes, i.e. stand alone strong entities, that can directly be managed.
- MOC3 hidden strong entity, that is managed elsewhere.
- OC5, a weak entity, that can only be managed in context of MOC1

Here the cardinalities and where the relation is managed:
```
MOC1 -0/1---n-> MOC2     managed in MOC1, MOC2
MOC1 -n-----m-> MOC3     managed in MOC1
MOC1 -n-----1-> MOC4     managed in MOC1
MOC1 -1-----n-> OC5      managed in MOC1
OC5  -n-----1-> MOC6     managed in OC5 from MOC1
```

## Attribute definition
Here the list of attributes with special treatment:
- MOC1::MOC2_List, MOC1::MOC3_List: editable MOC lists
- MOC2::MOC1: a MOC choice
- MOC1::OC5_List: a editable OC list
- MOC1::MOC4: selectable MOC (choice)
- MOC4::MOC1_List (non editable list)
    - Definitions in: schema.xml, GenadData.php, Moc4.php
- OC5::MOC6: a enum type of MOC6 (combo box)

## Starting the Application
see ```GUI/index.php```

