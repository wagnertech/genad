<?php
require_once 'GenericAdmin/GenericAdmin.php';

class GenadExample extends GenericAdmin
{
    function __construct() {
        // Generic Administration configuration data
        include 'AWK/GenadData.php';
        parent::setDscrData($mocd_tab, $atdc_tab);
    }
}

