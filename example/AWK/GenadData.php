<?php

// imports
require_once 'GenericAdmin/DataTypes/AttrProperty.php';

// Generic Administration configuration data
$mocd_tab = array (
    1 => array (
        'intern' => 'Moc1',
        'extern' => 'Moc1'),
    2 => array (
        'intern' => 'Moc2',
        'extern' => 'Moc2'),
    3 => array (
        'intern' => 'Moc3',
        'extern' => 'Moc3'),
    4 => array (
        'intern' => 'Moc4',
        'extern' => 'Moc4'),
    5 => array (
        'intern' => 'Oc5',
        'extern' => 'Oc5'),
);

$atdc_tab = array (
    array (
        'intern'  => 'id',
        'extern'  => 'id',
        'moc'     => 'Moc1',
        'props'   => array (AttrProperty::AP_GET, AttrProperty::AP_INIT)),
    array (
        'intern'  => 'att',
        'extern'  => 'att',
        'moc'     => 'Moc1',
        'props'   => array (AttrProperty::AP_GET, AttrProperty::AP_SET)),
    array (
        'intern'  => 'att',
        'extern'  => 'att',
        'moc'     => 'Moc4',
        'props'   => array (AttrProperty::AP_GET, AttrProperty::AP_SET),
    ),
    array (
        'intern'  => 'moc1_list',
        'extern'  => 'MOC1_List',
        'moc'     => 'Moc4',
        'props'   => array (AttrProperty::AP_GET),
        'att_base_type' => AttrBaseType::ABT_List,
        'rel_moc' => 1,
    ),
);

