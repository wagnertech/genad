<?php
$path = preg_replace("/genadex.*/", "genadex", __FILE__);
set_include_path($path . PATH_SEPARATOR . get_include_path());


require_once 'GenericAdmin/gui/control/GenadControl.php';

// load configuration + class loader
include 'GUI/ConfigurationData.php';
Config::setConfiguration($configurationData);

$gc = new GenadControl();
$gc->doIt();
