<?php
require_once 'util/Config.php';
require_once 'util/Logger.php';
require_once 'util/Transaction.php';
require_once 'util/XmlExtractor.php';
require_once 'util/XmlFormatter.php';
require_once 'GenericAdmin/impl/Adminstration.php';
require_once 'GenericAdmin/DataTypes/IGuiRepr.php';

class GenericAdmin {

	/** common error codes */
	const EC_OK			= 0; // ok
	const EC_NOSU_INST	= 1; // no such instance   
	const EC_DUPL_INST	= 2; // duplicate instance
	const EC_INV_REQU	= 3; // syntax implausibility of request data
	const EC_MOD_MISM	= 4; // modification status mismatch
	const EC_INV_OPER	= 5; // invalid operation (no create/set on attribute in class)
	const EC_ATT_MISS	= 6; // attribute missing in create request
	const EC_INV_FILT	= 7; // invalid filter in get request
	const EC_INT_VIOL	= 8; // referential integrity violation
	const EC_ATT_ERR	= 9; // malformed attribute
	
	private $mocd_tab = null;
	private $atdc_tab = null;
	protected $cur_app = "DEFAULT";
	
	function setDscrData($mocd_tab, $atdc_tab) {
		$this->mocd_tab = $mocd_tab;
		$this->atdc_tab = $atdc_tab;
		
		// compatibility code for attribute base type
		foreach ($this->atdc_tab as &$atdc) {
		    if (!isset($atdc['att_base_type'])){
		        $att_base_type = AttrBaseType::ABT_String; // default
		        $props = AttrProperties::fromArray($atdc['props']);
        		if ($props->incl(AttrProperty::AP_BOOL)) $att_base_type = AttrBaseType::ABT_Bool;
        		if ($props->incl(AttrProperty::AP_FLOAT)) $att_base_type = AttrBaseType::ABT_Float;
        		if ($props->incl(AttrProperty::AP_INT)) $att_base_type = AttrBaseType::ABT_Int;
        		if ($props->incl(AttrProperty::AP_LIST)) $att_base_type = AttrBaseType::ABT_List;
        		$atdc['att_base_type'] = $att_base_type;
		    }
		}
	}

	public function getMocDescList() {
		// logging
		$logger = Logger::getInstance();
		$logger->log(__CLASS__, "getMocDescList()");
		
		// call implementation via try/catch
		try {

			$ta = Transaction::getInstance($this->cur_app);
			$ta->startTA();
		
			$xmlFormatter = new XmlFormatter();
			$xmlFormatter->initResult();
			$erco = Administration::getMocDescList($this->mocd_tab, $xmlFormatter);

			$ta->commitTA();
			
			return $xmlFormatter->printIt();
			
		} catch (Exception $e) {
			$logger->logException(__CLASS__, $e);
			Transaction::getInstance($this->cur_app)->rollbackTA();
			throw $e;
		}
	}
	
	public function getAttrDescList($mocd_id) {
		// logging
		$logger = Logger::getInstance();
		$logger->log(__CLASS__, "getAttrDescList($mocd_id)");
		
		// call implementation via try/catch
		try {

			$ta = Transaction::getInstance($this->cur_app);
			$ta->startTA();
		
			$xmlFormatter = new XmlFormatter();
			$xmlFormatter->initResult();
			$mocd = $this->mocd_tab[$mocd_id];
			$erco = Administration::getAttrDescList($mocd, $this->atdc_tab, $xmlFormatter);

			$ta->commitTA();
			
			return $xmlFormatter->printIt();
			
		} catch (Exception $e) {
			$logger->logException(__CLASS__, $e);
			Transaction::getInstance($this->cur_app)->rollbackTA();
			throw $e;
		}
	}
	/*
	 * $req_filt = "<and><item co="equal"><att>value</att></item>...</and>"
	 * $odering  = "<oder><asc>att1</asc><desc>att2</desc>...</oder>"
	 */
	public function getInstanceList($mocd_id, $req_filt = null, $ordering = null) {
		// logging
		$logger = Logger::getInstance();
		$logger->log(__CLASS__, "getInstanceList($mocd_id)");
		$logger->log(__CLASS__, "req_filt($req_filt)");
		
		// call implementation via try/catch
		try {

			$ta = Transaction::getInstance($this->cur_app);
			$ta->startTA();
		
			if ($req_filt != null) {
				$xmlExtractor = new XmlExtractor();
				$xmlExtractor->openInput($req_filt);
			} else {
				$xmlExtractor = null;
			}
			
			$orderExtr = null;
			if ($ordering != null) {
			    $orderExtr = new XmlExtractor();
			    $orderExtr->openInput($ordering);
			}

			$xmlFormatter = new XmlFormatter();
			$xmlFormatter->initResult();
			$mocd = $this->mocd_tab[$mocd_id];
			$erco = Administration::getInstanceList($mocd, $this->atdc_tab, $xmlExtractor, $orderExtr, $xmlFormatter);
			if ($erco != GenericAdmin::EC_OK) throw new Exception("Administration::getInstanceList returned: $erco");

			$ta->commitTA();
			
			return $xmlFormatter->printIt();
			
		} catch (Exception $e) {
			$logger->logException(__CLASS__, $e);
			Transaction::getInstance($this->cur_app)->rollbackTA();
			throw $e;
		}
	}
	
	public function getInstance($mocd_id, $inst_id) {
		// logging
		$logger = Logger::getInstance();
		$logger->log(__CLASS__, "getInstance($mocd_id, $inst_id)");
		
		// call implementation via try/catch
		try {

			$ta = Transaction::getInstance($this->cur_app);
			$ta->startTA();
		
			$xmlFormatter = new XmlFormatter();
			$xmlFormatter->initResult();
			$mocd = $this->mocd_tab[$mocd_id];
			$erco = Administration::getInstance($this->mocd_tab, $this->atdc_tab, $mocd_id, $inst_id, $xmlFormatter);

			$ta->commitTA();
			
			if ($erco == self::EC_OK) {
				return $xmlFormatter->printIt();
			} else {
				return null;
			}
			
		} catch (Exception $e) {
			$logger->logException(__CLASS__, $e);
			Transaction::getInstance($this->cur_app)->rollbackTA();
			throw $e;
		}
	}
	
	public function getIdByName($mocd_id, $name) {
		// assumes an unique attribute called "name"
		// returns -1 for "not found"
		
		// logging
		$logger = Logger::getInstance();
		$logger->log(__CLASS__, "getIdFromName($mocd_id, $name)");
		
		// call implementation via try/catch
		try {

			$ta = Transaction::getInstance($this->cur_app);
			$ta->startTA();
		
			$mocd = $this->mocd_tab[$mocd_id];
			$id = Administration::getIdByName($this->mocd_tab, $mocd_id, $name);

			$ta->commitTA();
			
			return $id;
			
		} catch (Exception $e) {
			$logger->logException(__CLASS__, $e);
			Transaction::getInstance($this->cur_app)->rollbackTA();
			throw $e;
		}
	}
	
	public function setInstance($mocd_id, $req_par) {
		// logging
		$logger = Logger::getInstance();
		$logger->log(__CLASS__, "setInstance($mocd_id, $req_par)");
		
		// call implementation via try/catch
		try {

			$ta = Transaction::getInstance($this->cur_app);
			$ta->startTA();
		
			$xmlExtractor = new XmlExtractor();
			$xmlExtractor->openInput($req_par);
			$mocd = $this->mocd_tab[$mocd_id];
			$erco = Administration::setInstance($mocd, $this->atdc_tab, $xmlExtractor);

			$ta->commitTA();
			
			return $erco;
			
		} catch (Exception $e) {
			$logger->logException(__CLASS__, $e);
			Transaction::getInstance($this->cur_app)->rollbackTA();
			throw $e;
		}
	}
	
	public function createInstance($mocd_id, $req_par, &$id = null) {
		// logging
		$logger = Logger::getInstance();
		$logger->log(__CLASS__, "createInstance($mocd_id, $req_par)");
		
		// call implementation via try/catch
		try {
			// check, if autoloader in configuration
			$al = Config::getInstance()->getConfig("autoloader");
			if (!$al) Config::setConfiguration(array("autoloader" => array("ClassLoader", "autoload")));

			$ta = Transaction::getInstance($this->cur_app);
			$ta->startTA();
		
			$xmlExtractor = new XmlExtractor();
			$xmlExtractor->openInput($req_par);
			$mocd = $this->mocd_tab[$mocd_id];
			$erco = Administration::createInstance($mocd, $this->atdc_tab, $xmlExtractor, $id);

			if($erco == GenericAdmin::EC_OK)
				$ta->commitTA();
			else
				$ta->rollbackTA();
							
			return $erco;
			
		} catch (GenadException $gae) {
		    $logger->log(__CLASS__, "Problem: ".$gae->getMessage());
		    Transaction::getInstance($this->cur_app)->rollbackTA();
		    return $gae->getCode();
		} catch (Exception $e) {
			$logger->logException(__CLASS__, $e);
			Transaction::getInstance($this->cur_app)->rollbackTA();
			throw $e;
		}
	}
	
	public function deleteInstance($mocd_id, $inst_id, $mod) {
		// logging
		$logger = Logger::getInstance();
		$logger->log(__CLASS__, "deleteInstance($mocd_id, $inst_id, $mod)");
		
		// call implementation via try/catch
		try {

			$ta = Transaction::getInstance($this->cur_app);
			$ta->startTA();
		
			$mocd = $this->mocd_tab[$mocd_id];
			$erco = Administration::deleteInstance($mocd, $inst_id, $mod);

			$ta->commitTA();
			
			return $erco;
			
		} catch (Exception $e) {
			$logger->logException(__CLASS__, $e);
			Transaction::getInstance($this->cur_app)->rollbackTA();
			throw $e;
		}
	}
}
