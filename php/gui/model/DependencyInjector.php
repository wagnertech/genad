<?php
class DependencyInjector {

	// Singleton
	static $instance = null;

	static function getInstance() {
		if ( self::$instance == null ) {
			self::$instance = new Session();
		}
		return self::$instance;
	}
	
	private $viewHelper = null;
	
	public function getViewHelper() {
		return $this->viewHelper;
	} 
	
	public function setViewHelper($inst) {
		$this->viewHelper = $inst;
	}
	
}