<?php
//require_once 'gui/model/GenadConfig.php';
//require_once 'GenericAdmin/gui/control/GenadLoader.php';

class GenadIf {
	
	private $if_nam;
	private $interface;
	
	public function __construct($app) {
		//GenadLoader::loadInterface($app);
		$this->if_nam = Config::getInstance()->requireConfig("$app::InterfaceClass");
		//$am = new AlarmManagement();
		$class = new ReflectionClass($this->if_nam);
		$this->interface = $class->newInstance();
	}

	public function getAttrDescList($classId) {
		$if_func = new ReflectionMethod($this->if_nam, "getAttrDescList");
		return $if_func->invoke($this->interface, $classId);
	}
	
	public function getInstanceList($classId, $filter = null) {
		$if_func = new ReflectionMethod($this->if_nam, "getInstanceList");
		return $if_func->invoke($this->interface, $classId, $filter);
	}
	
	public function getClassDescriptorList() {
		$if_func = new ReflectionMethod($this->if_nam, "getMocDescList");
		return $if_func->invoke($this->interface, null);
	}
	
	public function getClassDescriptor($id) {
		require_once 'AlarmManagement/AlarmManagement.php';
		$if_func = new ReflectionMethod($this->if_nam, "getAttrDescList");
		return $if_func->invoke($this->interface, $id);
	}
	
	public function getInstance($moc_id, $id) {
		$if_func = new ReflectionMethod($this->if_nam, "getInstance");
		return $if_func->invoke($this->interface, $moc_id, $id);
	}

	public function performSet($moc_id, $xml) {
		$if_func = new ReflectionMethod($this->if_nam, "setInstance");
		$err = $if_func->invoke($this->interface, $moc_id, $xml);
		return $err;
	}

	public function performCreate($moc_id, $xml, &$id) {
		$if_func = new ReflectionMethod($this->if_nam, "createInstance");
		$err = $if_func->invokeArgs($this->interface, array($moc_id, $xml, &$id));
		return $err;
	}

	public function performDelete($moc_id, $id, $mod) {
		$if_func = new ReflectionMethod($this->if_nam, "deleteInstance");
		$err = $if_func->invoke($this->interface, $moc_id, $id, $mod);
		return $err;
	}
}
