<?php

require_once 'GenericAdmin/gui/model/GenadInstance.php';

class GenadInstanceList {
	
	private $app;
	private $className;
	private $classId;
	private $instanceList;
	
	public function __construct($app, $classId, $idList = null, $filter = null) {
		$this->app = $app;
		$this->classId = $classId;
		$this->className = GenadClassList::getInstance($app)->getClassName($classId);
		if ($idList === null) {
			$this->loadInstances($filter);
		}
		else {
			$this->instanceList = array();
			foreach ($idList as $id) {
				$inst = GenadInstance::load($app, $classId, $id);
				array_push($this->instanceList, $inst);
			}
		}
	}
		
	public function getClassId() {
		return $this->classId;
	}
	
	public function getClassName() {
		return $this->className;
	}
	
	public function getList() {
		return $this->instanceList;
	}
	
	public function containsId($id) {
		foreach ($this->instanceList as $inst) {
			if ($inst->getId() == $id) return true;
		}
		return false;
	}
	
	public function setList($arr) {
		$this->instanceList = $arr;
	}
	
	private function loadInstances($filter = null) {
		$this->instanceList = array();
		$if = new GenadIf($this->app);
		$insts_doc = $if->getInstanceList($this->classId, $filter);

		$instanceExtractor = new XmlExtractor();
		$instanceExtractor->openInput($insts_doc);
		
		// open instance list
		$instanceExtractor->extractElement(InputExtractor::EC_BEG, $elem, $value, $attrs);

		$desc_doc = $if->getAttrDescList($this->classId);
		$att_desc_ext = new XmlExtractor();
		$att_desc_ext->openInput($desc_doc);
		
		$cntl = InputExtractor::EC_BEG;
		while (($cntl = $instanceExtractor->extractElement($cntl, $elem, $value, $attrs)) != InputExtractor::EC_END) {
		    $att_desc_ext->toTop();
		    $instance = new GenadInstance($this->app, $this->classId, $att_desc_ext, $instanceExtractor);
		    array_push($this->instanceList, $instance);
		}
	}
}