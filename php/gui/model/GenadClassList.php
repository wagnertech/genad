<?php
require_once 'GenericAdmin/gui/model/interface/GenadIf.php';

class GenadClassList {
	
	// light weight pattern
	static private $inst_arr = array();
	
	static public function getInstance($app) {
		if (!isset(self::$inst_arr[$app])) {
			self::$inst_arr[$app] = new GenadClassList($app);
		}
		return self::$inst_arr[$app];
	}
	
	private $classList = array();
	private $propArray = array();
	private $app;
	
	public function getValue() {
		return $this->classList;
	}
	
	public function getMOCs() {
	    $mocs = array();
	    foreach ($this->classList as $id => $className) {
	        if ($this->hasProperty($id, new ClassProperty(ClassProperty::CP_Entity))) $mocs[$id] = $this->classList[$id];
	    }
	    return $mocs;
	}
	
	public function getClassName($id) {
		return $this->classList[$id];
	}
	
	public function getClassId($name) {
		return array_search($name, $this->classList);
	}
	
	public function hasProperty($id, ClassProperty $cp) {
		return $this->propArray[$id]->incl($cp);
	}
	
	private function __construct($app) {
		$this->app = $app;
		$this->load();
	}
	
	private function load() {
		$if = new GenadIf($this->app);
		$dom = $if->getClassDescriptorList();

		// extract data from, xml
		$instanceExtractor = new XmlExtractor();
		$instanceExtractor->openInput($dom);
		$instanceExtractor->extractElement(InputExtractor::EC_BEG, $elem, $value, $attrs);
		if ($elem != "ManagedObjectClassDescriptorList") throw new Exception("Unexpected xml tag: ".$elem);
		$ctl1 = InputExtractor::EC_BEG;
		while (($ctl1 = $instanceExtractor->extractElement($ctl1, $elem, $value, $attrs)) == InputExtractor::EC_CTN) {
			$id = $attrs["id"];
			$ctl2 = InputExtractor::EC_BEG;
			while (($ctl2 = $instanceExtractor->extractElement($ctl2, $elem, $value, $attrs)) == InputExtractor::EC_CTN) {
				if ($elem == "entity") $this->classList[$id] = $value;
				elseif ($elem == "props") $this->propArray[$id] = new ClassProperties($value);
				else throw new Exception("Unexpected xml tag: ".$elem);
			}
		}
	}
}