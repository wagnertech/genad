<?php
//require_once 'GenericAdmin/gui/control/GenadLoader.php';

class GenadAttribute {
	
	private $name;
	private $editable = false;
	private $systemInit = false; // a attribute, that is inited by the system
	private $value = "";
	private $attributeClass = 0; // for list attributes
	private $relatedClass = 0;
	private $attrProperties = null;
	private $typeClassName = "";
	private $typeClass = null;
	private $baseType = AttrBaseType::ABT_String;
	
	public function __construct($name="", $editable=false, $value="", $relatedClass = 0, $list = null) {
		if ($name == "")
			return;
		$this->name = $name;
		$this->editable = $editable;
		$this->value = $value;
		$this->relatedClass = $relatedClass;
		$this->list = $list;
	}
	
	public function getName() {
		return $this->name;
	}
	public function setName($value) {
		$this->name = $value;
	}
	
	public function isEditable() {
		return $this->editable;
	}
	
	public function hasSystemInit() {
		return $this->systemInit;
	}
	
	public function isList() {
	    return $this->baseType == AttrBaseType::ABT_List;
	}
	
	public function isFK() {
		return $this->relatedClass != 0;
	}
	
	public function isMOC() {
	    return ! $this->attrProperties->incl(new AttrProperty(AttrProperty::AP_NOMOC));
	}
	
	public function isDefiningFK() {
	    return $this->attrProperties->incl(new AttrProperty(AttrProperty::AP_DEF_FK));
	}
	
	public function isBool() {
	    return $this->baseType == AttrBaseType::ABT_Bool;
	}
	
	public function getValue() {
		return $this->value;
	}
	public function setValue($value = null) {
	    if ($this->isList()) $this->value = $value;
	    else if ($this->typeClass == null) {
		    if ($this->attrProperties->incl(new AttrProperty(AttrProperty::AP_FLOAT)))
		        $this->value = str_replace(",",".",$value);
		    else {
		        if ($value) $value = htmlentities($value, ENT_XML1);
			    $this->value = $value;
		    }
		}
		else {
		    if ($this->value instanceof $this->typeClassName) $this->value->init($value);
		    else $this->value = $this->typeClass->newInstance($value);
		}
	}
	public function setGuiRepr($value) {
	    if ($this->typeClass == null) {
	        if ($this->attrProperties->incl(new AttrProperty(AttrProperty::AP_FLOAT)))
	            $this->value = str_replace(",",".",$value);
	        else $this->value = htmlentities($value, ENT_XML1);
	    }
	    else {
	        $this->value->setGuiRepr($value);
	    }
	}
	public function getGuiRepr() {
	    $value = $this->value;
	    if ($this->typeClass != null) return $this->value->getGuiRepr();
	    if ($this->isList()) return "List($value)";
	    if ($this->isFK()) return "#$value";
	    if ($this->attrProperties->incl(new AttrProperty(AttrProperty::AP_FLOAT))){
	        if (empty($value)) return "0,0";
	        return str_replace(".",",",$value);
	    }
	    return $value;
	}
	public function getRelatedClass() {
		return $this->relatedClass;
	}
	public function setRelatedClass($value) {
		$this->relatedClass = $value;
	}
	public function getAttributeClass() {
	    return $this->attributeClass;
	}
	public function setAttributeClass($value) {
	    $this->attributeClass = $value;
	}
	
	public function setAttrProperties($value) {
		$this->attrProperties = $value;
		$this->systemInit = $this->attrProperties->incl(new AttrProperty(AttrProperty::AP_INIT));
		$this->editable = $this->attrProperties->incl(new AttrProperty(AttrProperty::AP_SET));
		if ($this->attrProperties->incl(new AttrProperty(AttrProperty::AP_TYP_CL_AWK)) // typ_cl is only used in AWK
		  && $this->typeClass != null //type class already set
		    ) {
            // reset typ_cl
		    $this->typeClass = null;
		    $this->value = null;
		}
	}
	public function setBaseType(int $abt) {
	    $this->baseType = $abt;
	}
	
	public function setTypeClass($app, $value) {
	    if ($this->attrProperties != null // already set
	        && $this->attrProperties->incl(new AttrProperty(AttrProperty::AP_TYP_CL_AWK)) // typ_cl is only used in AWK
	        ) return;
		$this->typeClassName = $value;
		$this->typeClass = new ReflectionClass($value);
	}
	public function getTypeClass() {
		return $this->typeClass;
	}
	public function resetTypeClass() {
	    $this->typeClass = null;
	}
	function getInputType() {
	    if ($this->typeClass) return $this->value->getInputType();
	    if ($this->baseType == AttrBaseType::ABT_EMail) return "email";
	    return "text";
	}
	
	public function recover() {
		if ($this->typeClassName != "") {
			$this->typeClass = new ReflectionClass($this->typeClassName);
		}
	}
    // returns a string as transport representation	
	public function __toString() {
		//if ($this->typeClass != null) return $this->value->__toString();
		return strval($this->value);
	}
}
