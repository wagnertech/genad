<?php
require_once 'GenericAdmin/gui/view/ErrorView.php';
require_once 'GenericAdmin/gui/view/ListView.php';
require_once 'GenericAdmin/gui/view/DetailView.php';
require_once 'GenericAdmin/gui/model/GenadInstanceList.php';

class GenadControl {

	const GC_EMPTY = 1;
	const GC_SAME = 2;
	const GC_COPY = 4;
	
	
	private $control_arr = array (
		"index" => array (
			"default" => "viewSelection"
			),
		"ListView" => array (
			"Info" => "info",
			"ClassSelect" => "listView",
			"default" => "viewSelection",
			"Edit" => "edit",
			"Copy" => "copy",
			"Add" => "add",
			"Delete" => "delete"
			),
		"DetailView" => array (
			"Edit" => "edit",
			"Info" => "info",
			"List" => "listView",
			"submit" => "submitDetailView",
			"Copy" => "copy",
			"create" => "create",
			"Delete" => "delete"
			),
		"RelatedListView" => array (
			"back" => "listView",
			"submit" => "submitRelatedListView"
			)
		);
	
	private $event;
	
	public function doIt() {
		// check page
	    $redirect_url = isset($_SERVER["REDIRECT_URL"]) ?  $_SERVER["REDIRECT_URL"] : "";
		$page = preg_replace("/.php$/", "", $redirect_url);
		$page = preg_replace("!^.*/!", "", $page);
		if ($page == "") $page = "index";
		
		// read request type
		//$requestMethod = $_SERVER["REQUEST_METHOD"];
		
		// read event
		if (isset($_GET["event"]))	$this->event = $_GET["event"];
		else $this->event = "default";
		
		if (!isset($this->control_arr[$page][$this->event]))
			throw new Exception ("No action registered for $page/".$this->event);
		$action = $this->control_arr[$page][$this->event];
			
		$method = new ReflectionMethod("GenadControl", $action);
		$method->invoke($this, null);
	}
	
	public function processAdmin() {
		switch ($event) {
		 case "ClassSelect":
		 	$listView = new ListView();
		 	$className = $_GET["class"];
		 	$instList = new GenadInstanceList($className);
		 	$listView->view($instList);
		 	break;
		 case "Info":
		 	break;
		 default:
		 	throw new Exception("Invalid event");
		}
	}

	public function listView() {
		$this->evalAppclass($app, $class);
		$listView = new ListView($app, $class);
		$xmlFormatter = new XmlFormatter();
		$xmlFormatter->initResult();
		$xmlFormatter->formatElement(OutputFormatter::FC_BEG, "and");
		if (array_key_exists("filter", $_GET)) {
	 	    $filter = $_GET["filter"];
	 		$f_arr = explode(" ", $filter);
	 		foreach ($f_arr as $f_item) {
	 			$f_elems = explode("-", $f_item);
 				$xmlFormatter->formatElement(OutputFormatter::FC_BEG, "item", null, array("co" => "equal"));
 				$xmlFormatter->formatElement(OutputFormatter::FC_SGL, $f_elems[0], $f_elems[1], null);
 				$xmlFormatter->formatElement(OutputFormatter::FC_END);
	 		}
	 	}
		$filterXml = $xmlFormatter->printIt();
	 	$instList = new GenadInstanceList($app, $class, null, $filterXml);
	 	$listView->view($instList);
	}
		
	public function create() {
		if ($_POST["create"]) {
			$this->evalAppclass($app, $class);
		 	
			// create empty instance
			$inst = GenadInstance::load($app, $class, 0);
			
			// update instance
			$inst->update($_POST);
			
			// send data to server
			$outputFormatter = new XmlFormatter();
			$inst->formatOutput($outputFormatter, false);
			$if = new GenadIf($app);
			$err = $if->performCreate($inst->getClassId(), $outputFormatter->printIt(), $id);
		 	if ($err == 0) {
			 	// reload page
			 	$appclass = $_GET["appclass"];
				header("Location:DetailView.php?event=List&appclass=$appclass");
				exit();
		 	}
		 	$view = new ErrorView($app);
	 		$view->view($err, $class, $id);
		}
		else throw new Exception("Invalid POST request");
	}

	public function delete() {
		$this->evalAppclass($app, $class);
		$id = $_GET["Id"];
		$mod = $_GET["mod"];
		$if = new GenadIf($app);
		$err = $if->performDelete($class, $id, $mod);
	 	if ($err == 0) {
		 	// reload page
		 	$appclass = $_GET["appclass"];
			header("Location:DetailView.php?event=List&appclass=$appclass");
			exit();
	 	}
	 	$view = new ErrorView($app);
 		$view->view($err, $class, $id);
		
	}
	
	
	public function submitDetailView() {
		if ($_POST["save"]) {
			$this->evalAppclass($app, $class);
			$id = $_GET["Id"];
			$mod = $_GET["mod"];
		 	
			// Hinweis für Debugger:
			// Setze $_GET["DEBUG"] = "true";
			// dann wird die Instance vom Server statt aus dem Cookie geholt.
			
			// update instance
			if (isset($_GET["DEBUG"])) {
				$if = new GenadIf($app);
				$inst = 	GenadInstance::load($app, $class, $id);
			}
			else {
				$inst = unserialize(Session::getInstance()->get("$app$class$id"));
				$inst->recover();
			}
			$inst->update($_POST);
			
			// send data to server
			$outputFormatter = new XmlFormatter();
			$inst->formatOutput($outputFormatter, true);
			$if = new GenadIf($app);
			$err = $if->performSet($inst->getClassId(), $outputFormatter->printIt());
		 	if ($err == 0) {
			 	// reload page
			 	$appclass = $_GET["appclass"];
				header("Location:DetailView.php?event=Info&appclass=$appclass&Id=$id");
				exit();
		 	}
		 	$view = new ErrorView($app);
	 		$view->view($err, $class, $id);
		}
		else throw new Exception("Invalid POST request");
	}
	
	public function submitRelatedListView() {
		if ($_POST["save"]) {
			$rel_cls = $_POST["related_class"];
			
			// TODO Aktuelle Werte aus Cache holen + zuvor schreiben
			$this->evalAppclass($app, $class);
			$id = $_GET["Id"];
			$mod = $_GET["mod"];
			$inst = GenadInstance::load($app, $class);
			
			// send data to server
			$inst->updateRelatedList($rel_cls, $_POST);
			$outputFormatter = new XmlFormatter();
			$inst->formatOutput($outputFormatter, true);
			$if = new GenadIf($app);
			$err = $if->performSet($class, $outputFormatter->printIt());
		 	if ($err == 0) {
			 	// reload page
			 	$appclass = $_GET["appclass"];
				header("Location:DetailView.php?event=Info&appclass=$appclass&Id=$id");
				exit();
		 	}
		 	$view = new ErrorView($app);
	 		$view->view($err, $class, $id);
		}
		// TODO: Edit main inst impl
		else throw new Exception("Invalid POST request");
	}
	
	public function viewSelection() {
		$view = new ListView();
		$view->viewSelection();
	}
	
	public function info() {
		$this->edi_inf(DetailView::DV_READ_ONLY);
	}

	public function edit() {
		$this->edi_inf(DetailView::DV_EDIT);
	}

	public function copy() {
		$this->edi_inf(DetailView::DV_NEW, self::GC_COPY);
	}
	
	public function add() {
		$this->edi_inf(DetailView::DV_NEW, self::GC_EMPTY);
	}
	
	private function edi_inf($mode, $inst_mode = self::GC_SAME) {
		$appclass = $_GET["appclass"];
		$rel_moi_id = isset($_GET["RMOI"]) ? $_GET["RMOI"] : "";
		$this->evalAppclass($app, $class);
		if ($inst_mode == self::GC_EMPTY) $id = 0;
		else {
			$id = $_GET["Id"];
			if ($id == 0) throw new Exception ("No instance defined.");
		}
		$detailView = new DetailView($app);
		
		// define instance
		$inst = GenadInstance::load($app, $class, $id, $rel_moi_id);
		if ($inst_mode == self::GC_COPY) {
			$inst->resetIdentity(); // reset id
			$id = 0;
		}
		$detailView->view($inst, $mode, $appclass);
		if ($mode != DetailView::DV_READ_ONLY) {
		    
		    // Instance in cache schreiben
		    $inst->resetTypeClassForSerialization();
		    Session::getInstance()->set("$app$class$id", serialize($inst));
		}
	}
	private function evalAppclass(&$app, &$class) {
	 	$appclass = $_GET["appclass"];
	 	$app = preg_replace("/ .*/", "", $appclass);
	 	$class = preg_replace("/.* /", "", $appclass);
	 	if ($app == "")
	 		throw new Exception("No valid application in request");
	 	if ($class == "")
	 		throw new Exception("No valid class in request");
	}
	
	/* this function checks whether the "Edit this list" button
	 * was pressed. 
	 * @result true: the button was pressed + the moc-id is returned
	 * @retult false: any other button was pressed
	 */
	private function relClassEdit($post_arr, &$rel_cls) {
		if ($_POST["EditRelList_1"]) {
				$rel_cls = 1;
				return true;
		}
		// TODO: richtige key-Funktion suchen
		//foreach (keys($post_arr) as $key) {
		foreach ($post_arr as $key) {
			// TODO durch substring-Funktion ersetzen
			if ($key == "Edit this list") {
				$rel_cls = 1;
				return true;
			}
			return false;
		}
	}
}