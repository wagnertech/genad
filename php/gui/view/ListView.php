<?php
require_once 'util/Config.php';
require_once 'GenericAdmin/gui/model/GenadClassList.php';
require_once 'GenericAdmin/gui/view/AdminView.php';

class ListView extends AdminView {
	
	private $class = 0;
	// in case this instance list belongs to a MOI
	private $rel_moi_id = -1;
	
	public function __construct($app="", $oc_id=0, $rel_moi_id=-1) {
		if ( $app != "")
		{
			$this->app = $app;
			$this->classList = GenadClassList::getInstance($app);
		}
		$this->class = $oc_id;
		$this->rel_moi_id = $rel_moi_id;
		parent::__construct();
	}
	
	// shows the start page of an MOC
	public function view(GenadInstanceList $instList) {

		//$this->class = $instList->getClassId();
		$this->vie_sel_pa1();
		$list = $this->getGuiString("List");
		$className = $this->getGuiString($instList->getClassName())." $list";
		
		print "<h3>$className</h3>";

		$this->viewList($instList);

		print "</body></html>";
		
	}
	
	private function vie_sel_pa1() {
		$app = $this->app;
		$this->printHeader();
		$this->printAdditionalNavigation();
		$classSelection = $this->getGuiString("Class Selection");
		$select = $this->getGuiString("Select");
		echo <<<PART1A
<h2>Administration</h2>

 <form id="mode_select" action="ListView.php">
 $classSelection:
 <button name="event" value="ClassSelect" title="Switch to selected class" id="mode-switch">$select</button>
 <select name="appclass" size="1">
 
PART1A;
		// loop over applications
		$app_list = Config::getInstance()->requireConfig("gui::applications");
		foreach ($app_list as $app) {
			// loop over classes of this application
			$classList = GenadClassList::getInstance($app)->getMOCs();
		    foreach($classList as $id => $className) {
				if ($this->app == $app and $this->class == $id)
					$sel_tag = 'selected="yes"';
				else
					$sel_tag = '';
				echo "  <option value=\"$app $id\" title=\"Create, edit or delete $className instances\" $sel_tag>$app | $className</option>\n";
			}
		}
		echo "</select>";
		echo "</form>\n";
	}

	// shows the admin page only with the class selection item 
	public function viewSelection() {
		$this->vie_sel_pa1();
		print "</body></html>";
	}
	
	/*
	 * viewList shows a list of instances
	 * $genAdList:		the list of instances
	 * $showIcons:		add the detail/edit/copy/delete-icons to each line
	 * $selectList:	add a selection icon in the front.
	 * $fk_name:		name of foreign key attribute
	 * $multiselect:	selection icons are radio buttons (false) or check boxes (true)
	 */
	public function viewList(GenadInstanceList $genAdList, $showIcons = true, GenadInstanceList $selectList = null, $fk_name = "", $multiselect = true) {
		if ($selectList) $selection = true; 
		else  $selection = false; 
		
		$inst_arr = $genAdList->getList();
		if (count($inst_arr) == 0) {
			$text = $this->getGuiString("NO_INSTANCE");
			echo "<p>$text</p>";
			if ($showIcons 
			    && $this->classList->hasProperty($this->class, new ClassProperty(ClassProperty::CP_CREATE))) {
				$appclass = $this->app . "+" . $this->class;
				echo <<<ADDKNOPF
<p>
	<a title="Add" href="ListView.php?event=Add&appclass=$appclass&Id=0&RMOI=$this->rel_moi_id"><img src="view/gfx/add.png" alt="Add"></a>
</p>
ADDKNOPF;
		}
			return;
		}
		echo "<table>";
		echo "<tr>\n";
		
		// fetch 1st instance
		$inst = $inst_arr[0];
		$atts = $inst->getAttributeList();
		if ($selection) echo "<th></th>";
		foreach ($atts as $att) {
			
			// print header line
			echo "<th>". $this->getGuiString($att->getName()) ."</th>";
		} 
		echo "</tr><tr>\n";
		
		if ($selection) $this->printSelection($fk_name, $inst->getId(), $selectList, $multiselect);

		// print attribute values of 1st instance
		foreach ($atts as $att) {
		    /* das mit "editable" lass ich weg. Dann ist alles grün und das Bild ruhiger
			if ($att->isEditable())
				$editable = ' class="edit"';
			else */
				$editable = '';

			if ($att->isBool()) {
				if ($att->getValue() == "true") $checked = " checked "; else $checked = "";
				echo "<td $editable>".'<input type="checkbox" name="'.
					$att->getName().'" value="true" disabled '.$checked.'/></td>';
			}
			else echo "<td$editable>".$att->getGuiRepr()."</td>";
		}
		if ($showIcons) $this->printIcons($inst->getId(), $inst->getMod());
		echo "</tr>\n";
		
		// print further instances
		for ($i=1; $i<count($inst_arr); $i++) {
			$inst = $inst_arr[$i];
			$atts = $inst->getAttributeList();
			if ($selection) $this->printSelection($fk_name, $inst->getId(), $selectList, $multiselect);
			foreach ($atts as $att) {
			    /*
				if ($att->isEditable())
					$editable = ' class="edit"';
				else */
					$editable = '';
				if ($att->isBool()) {
					if ($att->getValue() == "true") $checked = " checked "; else $checked = "";
					echo "<td $editable>".'<input type="checkbox" name="'.
						$att->getName().'" value="true" disabled '.$checked.'/></td>';
				}
				else echo "<td$editable>".$att->getGuiRepr()."</td>";
								}
			if ($showIcons) $this->printIcons($inst->getId(), $inst->getMod());
			echo "</tr>\n";
		}
		if ($showIcons) {
			$appclass = $this->app . "+" . $this->class;
			echo <<<LISTEND1

<tr>
	<td class="switch">
LISTEND1;
			if ($this->classList->hasProperty($this->class, new ClassProperty(ClassProperty::CP_CREATE)))
				echo "<a title=\"Add\" href=\"ListView.php?event=Add&appclass=$appclass&Id=0&RMOI=$this->rel_moi_id\"><img src=\"view/gfx/add.png\" alt=\"Add\"></a>";
			echo <<<LISTEND
	</td>
</tr>
</table>
LISTEND;
		}
		else {
			echo "</table>";
		}

	}
	
	private function printSelection($fk_name, $id, GenadInstanceList $selectList, $multiselect) {
		echo '<td><input type="';
		if ($multiselect) echo "checkbox\" name=\""."${fk_name}[]";
		else echo "radio\" name=\""."$fk_name";
		echo '" value="'."$id\"";
		if ($selectList->containsId($id)) echo " checked ";
		echo "/></td>";
	}
	
	private function printIcons($id, $mod) {
		$appclass = $this->app . "+" . $this->class;
		echo<<<ICONS
  <td class="switch">
 	 <a title="Info" href="ListView.php?event=Info&appclass=$appclass&Id=$id"><img src="view/gfx/info.png" alt="Info"></a>
ICONS;
		if ($this->classList->hasProperty($this->class, new ClassProperty(ClassProperty::CP_SET)))
 	 		echo "<a title=\"Edit\" href=\"ListView.php?event=Edit&appclass=$appclass&Id=$id\"><img src=\"view/gfx/edit.png\" alt=\"Edit\"></a>";
 	 	if ($this->classList->hasProperty($this->class, new ClassProperty(ClassProperty::CP_CREATE)))
    		echo "<a title=\"Copy\" href=\"ListView.php?event=Copy&appclass=$appclass&Id=$id\"><img src=\"view/gfx/copy.png\" alt=\"Copy\"></a>";
 	 	if ($this->classList->hasProperty($this->class, new ClassProperty(ClassProperty::CP_DELETE)))
    		echo "<a title=\"Delete\" href=\"ListView.php?event=Info&appclass=$appclass&Id=$id#deleteQuery\"><img src=\"view/gfx/delete.png\" alt=\"Delete\"></a>";
  		echo "</td>";
	}
   // <a title="Delete" href="ListView.php?event=Delete&appclass=$appclass&Id=$id&mod=$mod"><img src="view/gfx/delete.png" alt="Delete"></a>
	
	public function printAdditionalNavigation()
	{
		// 		require 'GenericAdmin/gui/view/NavBar.php';
	}
}


