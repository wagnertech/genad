<?php
class ErrorView {
	
	private $nameMap;
	private $app;

	public function ErrorView($app) {
		$this->app = $app;
		include "name_map.php";
	}
	
	//TODO: Diese Funktion in eine Superklasse auslagern.
	private function getGuiString($key) {
		$value = $this->nameMap[$key];
		if ($value == "") return $key;
		return $value;
	}
	
	public function view ($err=0, $class=0, $id=0) {
		echo<<<ERRVIEW
<html>
<head>
<title>SysAl Adminitration</title>
<link rel="stylesheet" type="text/css" href="view/default.css">
<link rel="stylesheet" type="text/css" href="view/admin.css">
</head>

<body>
<h1>Error occurred</h1>
ERRVIEW;
		if ($err != 0) {
			if ($err == GenericAdmin::EC_DUPL_INST) echo "<p>The instance you try to create is already existing</p>";
			else if ($err == GenericAdmin::EC_ATT_ERR) echo "<p>An Attribute you entered is malformed.</p>";
			else if ($err == GenericAdmin::EC_ATT_MISS) echo "<p>The instance you try to create has missing attributes</p>";
			else if ($err == GenericAdmin::EC_INT_VIOL) echo "<p>The action performed led to a referential integrity problem</p>";
			else echo "<p>The error id is $err.</p>";
		}
			
			
		if ($class != 0 && $id == 0) {
			$appclass = $this->app." ".$class;			
			echo<<<CONT1
<p>
<form method="GET" action="DetailView.php">
<input type="hidden" name="event" value="List"/>
<input type="hidden" name="appclass" value="$appclass"/>
<input type="submit" value="Continue"/>
</form></p>
CONT1;
		}
		if ($class != 0 && $id != 0) {
			$appclass = $this->app." ".$class;			
			echo<<<CONT2
<p>
<form method="GET" action="DetailView.php">
<input type="hidden" name="event" value="Info"/>
<input type="hidden" name="appclass" value="$appclass"/>
<input type="hidden" name="Id" value="$id"/>
<input type="submit" value="Continue"/>
</form></p>
CONT2;
		}
		echo "</body></html";	
	}
}
	