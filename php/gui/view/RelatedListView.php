<?php
class RelatedListView extends AdminView {
	
	private $class;
	
	function __construct($app, $class) {
		$this->app = $app;
		$this->class = $class;
	}

	function view(GenadInstance $inst, GenadInstanceList $rel_lst) {
		// view page header
		$this->printHeader();
		
		// view instance header
		$name = $inst->getName();
		echo "<h3>Main $name instance</h3>";
		$listView = new ListView($this->app);
		$inst_lst = new GenadInstanceList($this->app, $this->class);
		$inst_arr = array($inst);
		$inst_lst->setList($inst_arr);
		$listView->viewList($inst_lst, false);
		echo "<hr>";
		
		// check for relation attribute
		$att_arr = $inst->getAttributeList();
		$selectedIdList = array();
		$rel_cls = $rel_lst->getClassId();
		foreach ($att_arr as $att) {
			if ($att->getRelatedClass() == $rel_cls)
				$selectedIdList = $att->getList();
		}
		// TODO: Kann ich hier "leere Liste" von "keine Liste" unterscheiden?
		//if (!$selectedIdList)
		//	throw new Exception("Related attribute list not found");
			
		// view list of releated instances
		$name = $rel_lst->getClassName();
		$inst_arr = $rel_lst->getList();
		$appclass = $this->app." ".$inst->getClassId();
		$id = $inst->getId();
		$mod = $inst->getMod();
		echo <<<START
<h3>Edit related $name list</h3>
<form method="POST" action="RelatedListView.php?event=submit&appclass=$appclass&Id=$id&mod=$mod">
<input type="hidden" name="related_class" value="$rel_cls"/>
<table><tr>
START;
		// fetch 1st instance
		$i = $inst_arr[0];
		$atts = $i->getAttributeList();
		echo "<th></th>";
		foreach ($atts as $att) {
			
			// print header line
			echo "<th>". $this->getGuiString($att->getName()) ."</th>";
		} 
		echo "</tr>\n";
		
		foreach ($inst_arr as $i) {
			$selected = false;
			$id = $i->getId();
			// TODO hasElement verwenden
			if (array_search($id, $selectedIdList))
				$selected = "yes";
				
			// TODO richtige checkbox setzen
			echo "<tr><td><input type=\"checkbox\" name=\"$id\" value=\"1\" selected=\"$selected\"></td>";
			$atts = $i->getAttributeList();
			foreach ($atts as $att) {
				echo "<td>".$att."</td>";
			}
			echo "</tr>\n";
		}
		echo "</table>\n";
		$appclass = $this->app." ".$inst->getClassId();
		echo <<<LISTEND
<p>
<input type="submit" name="continueEdit" value="Edit main instance">
<input type="submit" name="save" value="Commit changes">
</p>
</form>
<form method="GET" action="RelatedListView.php">
<input type="hidden" name="event" value="back"/>
<input type="hidden" name="appclass" value="$appclass"/>
<input type="submit" value="Back"/>
</form>
</body>
</html>
LISTEND;
	}
}
	