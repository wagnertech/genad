<?php
class AdminView {

	protected $nameMap = array();
	protected $app = "";

	protected function __construct() {
	    if (file_exists('name_map.php')) include 'name_map.php';
	}
	protected function getGuiString($key) {
	    if (array_key_exists($key, $this->nameMap)) return $this->nameMap[$key];
		return $key;
	}
	
	protected function printHeader() {
		$app = $this->app;
		echo <<<PART1
<html>
<head>
<title>$app WebGUI</title>
<link rel="stylesheet" type="text/css" href="view/admin.css">
<meta http-equiv="Content-Type" content="text/html; charset=utf8">
</head>
<body>
<!-- TODO: Lokalisierungsmöglichkeit einbauen <? php // echo $ guiText->getText("STRING_"); ?> -->
<p><a href="index.php">Home</a></p><h1>$app System</h1>
<hr>
PART1;
		
	}
}