<?php
require_once 'GenericAdmin/gui/view/ListView.php';
require_once 'GenericAdmin/gui/model/GenadClassList.php';

class DetailView extends AdminView {
	
	// Detail view can be viewed in following modes:
	const DV_NEW = 1;
	const DV_READ_ONLY = 2;
	const DV_EDIT = 3;
	
	public function __construct($app) {
		$this->app = $app;
		parent::__construct();
	}
	
	public function view(GenadInstance $inst, $mode, $requestAppclass = null) {
		$this->printHeader();
		$className = $inst->getName();
		$appclass = $this->app." ".$inst->getClassId();
		if (!$requestAppclass) $requestAppclass = $appclass;
		$id = $inst->getId();
		$mod = $inst->getMod();
		echo <<<DELETE_QUERY
  <!-- check box for delete query -->
  <div id="deleteQuery" class="overlay">
	 <div class="popup">
		<p>Really delete this instance?</p>
		<a class="button" href="#">No</a>&nbsp;&nbsp;&nbsp;<a class="button" href="ListView.php?event=Delete&appclass=$appclass&Id=$id&mod=$mod">Yes</a>
	 </div>
  </div>
DELETE_QUERY;
		
		echo "<h1>$className</h1>";
		if ($mode == self::DV_NEW)
			echo '<form method="POST" action="DetailView.php?event=create&appclass='."$requestAppclass&Id=$id&mod=$mod".'">';
		elseif ($mode == self::DV_EDIT)
			echo '<form method="POST" action="DetailView.php?event=submit&appclass='."$requestAppclass&Id=$id&mod=$mod".'">';
			
		echo "\n<table>";

		$specialAttributeExists = false;
		$atts = $inst->getAttributeList();
		foreach ($atts as $att) {
			if (($att->isList() || $att->isFK()) && ! $att->isDefiningFK())
				$specialAttributeExists = true;
			else { // it's a scalar attribute
			    if (($att->isEditable() && $mode == self::DV_EDIT) || $mode == self::DV_NEW && (! $att->hasSystemInit()) && (! $att->isDefiningFK())) {
					if ($mode == self::DV_READ_ONLY) {
						
						// attribute in principle editable, but no editing mode
						if ($att->isBool()) {
							if ($att->getValue() == "true") $checked = " checked "; else $checked = "";
							echo "<tr><th>".$this->getGuiString($att->getName()).'</th><td class="edit"><input type="checkbox" name="'.
								$att->getName().'" value="true" disabled '.$checked.'/></td></tr>';
						}
						else {
							echo "<tr><th>".$this->getGuiString($att->getName()).'</th><td class="edit">'.$att."</td></tr>\n";
						}						
					} 
					else {
						// check type class
						if ( ($typ_cl = $att->getTypeClass()) != null 
						    && method_exists($att->getValue(), "GetDefdVals")) { // a enum attribute
						    
							$tc_inst = $att->getValue();
						    $defd_vals = $tc_inst->GetDefdVals();
							echo "<tr><th>".$this->getGuiString($att->getName()).
								'</th><td class="edit"><select name="'.$att->getName().'">';
							foreach ($defd_vals as $key => $val) {
								$selected = "";
								if ($val == $att->getGuiRepr()) $selected = ' selected="yes"';
								echo '<option value="'.$val."\"$selected>".$val.'</option>';
							}
							echo '</select></td></tr>';
						}
						else if ($att->isBool()) {
							if ($att->getValue() == "true") $checked = " checked "; else $checked = "";
							echo "<tr><th>".$this->getGuiString($att->getName()).'</th><td class="edit"><input type="checkbox" name="'.
								$att->getName().'" value="true"'.$checked.'/></td></tr>';
						}
						else {
						    $input_type = $att->getInputType();
							echo "<tr><th>".$this->getGuiString($att->getName()).'</th><td class="edit"><input type="'.$input_type.'" required name="'.
								$att->getName().'" value="'.$att->getGuiRepr().'"/></td></tr>';
						}
					}
				}
				else { // a not editable attribute and not CREATE mode
					if ($att->isBool()) {
						if ($att->getValue() == "true") $checked = " checked "; else $checked = "";
						echo "<tr><th>".$this->getGuiString($att->getName()).'</th><td><input type="checkbox" name="'.
							$att->getName().'" value="true" disabled '.$checked.'/></td></tr>';
					}
					else echo "<tr><th>".$this->getGuiString($att->getName())."</th><td>".$att->getGuiRepr()."</td></tr>\n";
					if ($att->isDefiningFK()) {
					    // mandatory FK, already set -> transfer as hidden field
					    echo '<input type="hidden" name="'.$att->getName().'" value="'.$att->getValue().'"/>';
					    
					}
				}
				
			}
				
		}			

		echo "</table>\n";
		
		if ($specialAttributeExists) {
			// process simple relation attributes (n:1)
		    foreach ($atts as $att) {
				if ($att->isFK() && !$att->isList() && ! $att->hasSystemInit()) {
					$idList = array($att->getValue());
					$rel_moc = $att->getRelatedClass();
					$instList = new GenadInstanceList($this->app, $rel_moc, $idList);
					echo "<h3>Related ".$att->getName()."</h3>";
					$listView = new ListView($this->app);
					if (($att->isEditable() && ($mode == self::DV_EDIT)) || ($mode == self::DV_NEW)) {
						$totalList = new GenadInstanceList($this->app, $rel_moc);
						$att_nam = $this->getGuiString($att->getName());
						$listView->viewList($totalList, false, $instList, $att_nam, false);
					}
					else $listView->viewList($instList, false);
				}
			}
			// process simple lists (list attributes). Values are directly given
			foreach ($atts as $att) {
				if (!$att->isFK() && $att->isList()) {
				    if ($mode == self::DV_NEW) ; // do nothing
				    else {
				        // show list
				        $att_appclass = "$this->app+".$att->getAttributeClass();
				        $att_nam = $this->getGuiString($att->getName());
				        echo "<h3>$att_nam List</h3>\n";
				        echo "<table>";
				        
				        $list = $att->getList();
				        foreach ($list as $elem) {
				            echo "<tr><td>$elem</td>";
				            if ($mode == self::DV_READ_ONLY && $att->isEditable()) {
				                // add edit icons
				                echo "<a title=\"Edit\" href=\"DetailView.php?event=Edit&appclass=$att_appclass&Id=$id\"><img src=\"view/gfx/edit.png\" alt=\"Edit\"></a>";
			                    echo "<a title=\"Copy\" href=\"ListView.php?event=Copy&appclass=$att_appclass&Id=$id\"><img src=\"view/gfx/copy.png\" alt=\"Copy\"></a>";
		                        echo "<a title=\"Delete\" href=\"ListView.php?event=Info&appclass=$att_appclass&Id=$id#deleteQuery\"><img src=\"view/gfx/delete.png\" alt=\"Delete\"></a>";
				            }
				            echo "</tr>\n";
				        }
				        echo "</table>";
				        if ($mode == self::DV_READ_ONLY && $att->isEditable()) {
				            // add ADD icon
				            echo '<a title="Add" href="ListView.php?event=Add&appclass=$appclass&Id=0"><img src="view/gfx/add.png" alt="Add"></a>';
				        }
				    }
				}
			}
				
			// process foreign key lists (n:m)
			foreach ($atts as $att) {
			    if ($att->isFK() && $att->isList()&& $att->isMOC()) {
					$idList = $att->getValue();
					if ($idList === null) $idList = array(); // this happens in empty create
					$rel_moc = $att->getRelatedClass();
					$instList = new GenadInstanceList($this->app, $rel_moc, $idList);
					echo "<h3>Related ".$att->getName()." List</h3>";
					$listView = new ListView("$this->app");
					if (($att->isEditable() && ($mode == self::DV_EDIT)) || ($mode == self::DV_NEW)) {
						$totalList = new GenadInstanceList($this->app, $rel_moc);
						$att_nam = $this->getGuiString($att->getName());
						$listView->viewList($totalList, false, $instList, $att_nam, true);
					}
					else $listView->viewList($instList, false);
/*					$att_nam = $this->getGuiString($att->getName());
					echo "<h3>Related $att_nam List</h3><p>\n";
					$idList = $att->getList();
					$rel_moc = $att->getRelatedClass();
					$instList = new GenadInstanceList($this->app, $rel_moc, $idList);
					$listView = new ListView($this->app);
					$listView->viewList($instList, false);
					if ($mode == self::DV_EDIT) {
						echo "<input type=\"submit\" name=\"EditRelList_$rel_moc\" value=\"Edit this list\"/>";
					}
					echo "</p>\n"; */
				}
			}
			
			print "<hr/>\n";
		}

		if ($mode == self::DV_READ_ONLY) {
			echo "<p/>\n";
    		$classList = GenadClassList::getInstance($this->app);
    		$classId = $inst->getClassId();
    		if ($classList->hasProperty($classId, new ClassProperty(ClassProperty::CP_SET)))
     	 		echo "<a title=\"Edit\" href=\"DetailView.php?event=Edit&appclass=$appclass&Id=$id\"><img src=\"view/gfx/edit.png\" alt=\"Edit\"></a>";
     	 	if ($classList->hasProperty($classId, new ClassProperty(ClassProperty::CP_CREATE)))
        		echo "<a title=\"Copy\" href=\"ListView.php?event=Copy&appclass=$appclass&Id=$id\"><img src=\"view/gfx/copy.png\" alt=\"Copy\"></a>";
     	 	if ($classList->hasProperty($classId, new ClassProperty(ClassProperty::CP_DELETE)))
        		echo "<a title=\"Delete\" href=\"ListView.php?event=Info&appclass=$appclass&Id=$id#deleteQuery\"><img src=\"view/gfx/delete.png\" alt=\"Delete\"></a>";
      		echo "</td>";
      		
		}
		else {
			if ( $mode == self::DV_NEW ) {
				$label = $this->getGuiString("Create");
				$req = "create";
			} else {
				$label = $this->getGuiString("Submit changes");
				$req = "save";
			}
			echo '<input type="submit" name="' . $req . '" value="' . $label . '"/>';
			echo "</form>\n";
		}

		// process foreign key lists (1:n)
		foreach ($atts as $att) {
		    if ( $mode == self::DV_NEW ) ; // in this case no depending list is displayed
		    else {
		        if ($att->isList() && $att->isFK() && ! $att->isMOC()) {
    		        $idList = $att->getList();
    		        if ($idList === null) $idList = array(); // this happens in empty create
    		        $rel_moc = $att->getRelatedClass();
    		        $instList = new GenadInstanceList($this->app, $rel_moc, $idList);
    		        echo "<h3>".$att->getName()." Member List</h3>";
    		        $listView = new ListView("$this->app", $rel_moc, $id);
    		        /*
    		        if (($att->isEditable() && ($mode == self::DV_EDIT)) || ($mode == self::DV_NEW)) {
    		            $totalList = new GenadInstanceList($this->app, $rel_moc);
    		            $att_nam = $this->getGuiString($att->getName());
    		            $listView->viewList($totalList, false, $instList, $att_nam, true);
    		        }
    		        else */$listView->viewList($instList, true);
    		        /*					$att_nam = $this->getGuiString($att->getName());
    		         echo "<h3>Related $att_nam List</h3><p>\n";
    		         $idList = $att->getList();
    		         $rel_moc = $att->getRelatedClass();
    		         $instList = new GenadInstanceList($this->app, $rel_moc, $idList);
    		         $listView = new ListView($this->app);
    		         $listView->viewList($instList, false);
    		         if ($mode == self::DV_EDIT) {
    		         echo "<input type=\"submit\" name=\"EditRelList_$rel_moc\" value=\"Edit this list\"/>";
    		         }
    		         echo "</p>\n"; */
		        }
		    }
		}
		
		
		$back = $this->getGuiString("Back");
		echo<<<END1
<p/>
<form method="GET" action="DetailView.php">
<input type="hidden" name="event" value="List"/>
<input type="hidden" name="appclass" value="$requestAppclass"/>
<input type="submit" value="$back"/>
</form>
</body>
</html>
END1;
	}
}
