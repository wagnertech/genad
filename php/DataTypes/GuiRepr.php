<?php
require_once 'GenericAdmin/DataTypes/IGuiRepr.php';

class GuiRepr implements \IGuiRepr
{
    protected $value;
    
    function __construct($s) {
        $this->value = $s;
    }
    
    public function init(string $s) {
        $this->value = $s;
    }

    public function getInputType() {
        return "text";
    }

    public function setGuiRepr(string $s) {
        $this->value = $s;
    }

    public function getPattern() {
        return null;
    }

    public function getGuiRepr() {
        return $this->value;
    }
    
    function __toString() {
        return $this->value;
    }
}

