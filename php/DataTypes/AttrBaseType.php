<?php
class AttrBaseType
{
    // possible values for attribute base types
    const ABT_String = 1; // default
    const ABT_Int    = 2;
    const ABT_Float  = 3;
    const ABT_Bool   = 4;
    const ABT_EMail  = 5;
    const ABT_List   = 6;
}

