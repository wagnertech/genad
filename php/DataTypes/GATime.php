<?php

class GATime extends GuiRepr
{
    public function __construct($s) {
        if ($s) $this->setGuiRepr($s);
    }
    public function init(string $s = "") {
        $this->setGuiRepr($s);
    }
    public function getValue() {
        return $this->value;
    }
    public function __toString() {
        $dt = new DateTime();
        $dt->setTimezone(new DateTimeZone("UTC"));
        $dt->setTimestamp($this->value);
        return $dt->format("H:i:s");
    }
    public function getGuiRepr() {
        return $this->__toString();
    }
    public function setGuiRepr(string $s) {
        $i = intval($s);
        if (preg_match("/(\d{1,2}):(\d\d):(\d\d)/", $s, $matches)) {
            $i = $matches[1]*3600 + $matches[2]*60 + $matches[3];
        }
        elseif (preg_match("/(\d{1,2}):(\d\d)/", $s, $matches)) {
            $i = $matches[1]*60 + $matches[2];
        }
        $this->value = $i;
    }
}

