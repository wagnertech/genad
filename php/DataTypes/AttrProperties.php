<?php
require_once 'GenericAdmin/DataTypes/AttrProperty.php';

class AttrProperties {
	
	private $value = 0;
	
	public static function fromArray($vals = array()) {
		$props = new AttrProperties();
		foreach ($vals as $val) {
			$props->accu(new AttrProperty($val));
		}
		return $props;
	}
	
	public function __construct($val = 0) {
		$this->value = $val;
	}
	
	public function getValue() {
		return $this->value;
	}
	
	public function accu(AttrProperty $attr_prop) {
		$this->value = $this->value | pow(2,$attr_prop->getValue());
	}
	
	public function incl($attr_prop) {
	    if ($attr_prop instanceof AttrProperty) $attr_prop = $attr_prop->getValue();
	    return 0 !== ($this->value & pow(2,$attr_prop));
	}
	
	public function __toString() {
		return "$this->value";
	}
}
