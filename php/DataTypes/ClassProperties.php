<?php
require_once 'GenericAdmin/DataTypes/ClassProperty.php';

class ClassProperties {
	
	private $value = 0;
	
	public static function fromArray($vals = array()) {
		$props = new ClassProperties();
		foreach ($vals as $val) {
			$props->accu(new ClassProperty($val));
		}
		return $props;
	}
	
	public function __construct($val = 0) {
		$this->value = $val;
	}
	
	public function getValue() {
		return $this->value;
	}
	
	public function accu(ClassProperty $class_prop) {
		$this->value = $this->value | pow(2,$class_prop->getValue());
	}
	
	public function incl($class_prop) {
	    if ($class_prop instanceof ClassProperty) $class_prop = $class_prop->getValue();
		return 0 !== ($this->value & pow(2,$class_prop));
	}
	
	public function __toString() {
		return "$this->value";
	}
}
