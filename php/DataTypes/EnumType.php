<?php
// imports
require_once 'GenericAdmin/DataTypes/FilterTag.php';
require_once 'GenericAdmin/DataTypes/GuiRepr.php';

class EnumType extends GuiRepr {
	// subclasses has to define value constants and a constructor which 
	// has to call the initInstance method to initialize the instance data

	private $type_name;			   // class name of the type
	private $defd_vals = array (); // defined values (array of key value => string) 

	protected function initInstance($init_type_name, $init_defd_vals, $init_value) {
		$this->type_name = $init_type_name;
		$this->defd_vals = $init_defd_vals;
		
		if ($init_value === null) { //default value requested
			$this->value = min(array_keys($init_defd_vals));
		} elseif (is_string($init_value) && 
			($vkey = array_search($init_value, $this->defd_vals)) !== false) {
			$this->value = $vkey;
		} elseif (array_key_exists ($init_value, $this->defd_vals)) {
			$this->value = $init_value;
		} else {
			throw new Exception("Invalid $this->type_name creation: $init_value");
		}
	}

	public function getValue() {
		if (isset($this->defd_vals[$this->value])) {
			return $this->value;
		} else {
			throw new Exception("Invalid $this->type_name value (gotten): $this->value");
		}
	}
	public function GetDefdVals() {
	    return $this->defd_vals;
	}
	public function __toString() {
		if (isset($this->defd_vals[$this->value])) {
			return $this->defd_vals[$this->value];
		} else {
			throw new Exception("Invalid $this->type_name value: $this->value");
		}
	}
	public function compValue($comp_tag, $str_repr) {
		if (!(is_string($str_repr) && ($comp_val = array_search($str_repr, $this->defd_vals)) !== false))
			throw new Exception("Invalid $this->type_name in filter comparison: $str_repr");
		
		switch ($comp_tag) {
			case FilterTag::FC_EQUAL:
				return ($this->value == $comp_val);
			case FilterTag::FC_LESS:
				return ($this->value < $comp_val);
			case FilterTag::FC_GREATER:
				return ($this->value > $comp_val);
		}
	}
	
	public function equals(EnumType $e) {
		return ($this->value == $e->value);
	}
	public function getGuiRepr() {
	    return $this->__toString();
	}
	public function init(string $s) {
	    $this->initInstance($this->type_name, $this->defd_vals, $s);
	}
	public function setGuiRepr(string $s) {
	    $this->init($s);
	}
}
