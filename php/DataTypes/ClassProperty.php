<?php
class ClassProperty {
	/** the possible values for attribute class property */
	const CP_CREATE  = 1; // create
	const CP_SET     = 2; // set
	const CP_DELETE  = 3; // delete
	const CP_Entity  = 4; // OC is entity
	
	private $value;
	
	public function __construct($val) {
		if (self::CP_CREATE <= $val && $val <= self::CP_Entity)
			$this->value = $val;
		else
			throw new Exception("Invalid ClassProperty creation: $val");
	}
	
	public function getValue() {
		return $this->value;
	}
}
