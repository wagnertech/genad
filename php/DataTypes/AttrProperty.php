<?php
class AttrProperty {
	/** the possible values for attribute attribute property */
	const AP_GET        = 1; // get
	const AP_SET        = 2; // set
	const AP_LIST       = 3; // obsolete
	const AP_NOMOC      = 5; // related class is no MOC/Entity
	const AP_BOOL       = 6; // obsolete
	const AP_INT        = 7; // obsolete
	const AP_FLOAT      = 8; // obsolete
	const AP_INIT       = 9; // initial value by backend
	const AP_DEF_FK     = 10; // FK that defines this weak entity
	const AP_DEF        = 11; // default value
	const AP_TYP_CL_AWK = 12; // default value
	const AP_MEQU       = 13; // matching for equality (in filter items)
	const AP_MORD       = 14; // matching for ordering (in filter items)
	const AP_REL        = 15; // obsolete, no effect
	const AP_MAX        = 15;
	
	// 1:n = AP_LIST,AP_NOMOC + rel_moc. Think of cascaded delete in the dll!
	// n:1 = rel_moc
	// n:m = AP_LIST + rel_moc
	
	private $value;
	
	public function __construct($val) {
		if (self::AP_GET <= $val && $val <= self::AP_MAX)
			$this->value = $val;
		else
			throw new Exception("Invalid AttrProperty creation: $val");
	}
	
	public function getValue() {
		return $this->value;
	}
}
