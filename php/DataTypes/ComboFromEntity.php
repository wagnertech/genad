<?php
// this class must inly be used inside GenadGUI. It uses GenadGUI model classes

class ComboFromEntity extends EnumType
{
    public function __construct(string $app, int $moc_id, string $column_name, $value = null) {
        $inst_list = new GenadInstanceList($app, $moc_id);
        $defd_vals = array();
        foreach ($inst_list->getList() as $inst) {
            $id = $inst->getId();
            $att_list = $inst->getAttributeList();
            foreach ($att_list as $att) {
                if ($att->getName() == $column_name) $defd_vals[$id] = $att->getValue();
            }
        }
        if (count($defd_vals) == 0) $defd_vals[0] = "default";
        $this->initInstance('Status', $defd_vals, $value);
    }
    public function getGuiRepr() {
        return parent::__toString();
    }
    public function __toString() {
        return "".$this->value;
    }
    
}

