<?php

interface IGuiRepr
{
    // init instance from transport representation
    public function init(string $s);
    
    // return GUI representation
    public function getGuiRepr();
    
    // init instace from GUI representation
    public function setGuiRepr(string $s);
    
    // get input type
    public function getInputType();
    
    // get regex pattern
    public function getPattern();
}

