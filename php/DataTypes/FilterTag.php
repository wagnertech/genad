<?php

class FilterTag {

	/** the possible values for filter node tags */
	const FN_ITEM = 1; // item
	const FN_AND  = 2; // and
	const FN_OR   = 3; // or
	const FN_NOT  = 4; // not

	/** the possible values for filter (item) comparator tags */
	const FC_PRESENT = 1; // present
	const FC_EQUAL 	 = 2; // equal
	const FC_LESS  	 = 3; // less
	const FC_GREATER = 4; // greater

	// (string representations of) filter node tags
	public static $fNodeTags = array (
		self::FN_ITEM => 'item',
		self::FN_AND  => 'and',
		self::FN_OR   => 'or',
		self::FN_NOT  => 'not'
	);

	// (string representations of) filter (item) comparator tags
	public static $fCompTags = array (
		self::FC_PRESENT => 'present',
		self::FC_EQUAL   => 'equal',
		self::FC_LESS    => 'less',
		self::FC_GREATER => 'greater'
	);

	public static function toFNodeTagVal($strRepr) {
		if (is_string($strRepr) && ($vkey = array_search($strRepr, self::$fNodeTags)) !== false) {
			return $vkey;
		} else {
			throw new Exception("Invalid filter node tag: $strRepr");
		}
	}

	public static function toFCompTagVal($strRepr) {
		if (is_string($strRepr) && ($vkey = array_search($strRepr, self::$fCompTags)) !== false) {
			return $vkey;
		} else {
			throw new Exception("Invalid filter comperator tag: $strRepr");
		}
	}
}
