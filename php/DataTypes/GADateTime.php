<?php

class GADateTime extends GuiRepr
{
    public function __construct($i) {
        if ($i) $this->init($i);
    }
    public function getValue() {
        return $this->value;
    }
    public function __toString() {
        return date("Y-m-d H:i:s", $this->value);
    }
    public function init(string $s) {
        $this->value = intval($s);
    }
    public function getGuiRepr() {
        return $this->__toString();
    }
    public function setGuiRepr(string $s) {
        throw new RuntimeException("NIY");
    }
}

